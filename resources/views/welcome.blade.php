@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark mb-3">
      <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic border-bottom">Blog de <br>{{ Auth::user()->name }}</h1>
      </div>
    </div>
    <div class="row">
      @foreach ($posts as $post)            
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img src="{{ $post->image }}" class="bd-placeholder-img card-img-top" width="300" height="200">
            <div class="card-body">
              <h3 class="display-6 font-italic">{{ $post->title }}</h3>
              {{--substr — Devuelve parte de una cadena(primero se pasa el texto y segundo la longitud)--}}
              <p class="card-text">{{ substr($post->body, 0,40) }}...</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group  mb-1">
                  <a href="{{ 'post' }}/{{ $post->slug }}" class="btn btn-sm btn-outline-secondary btn-primary text-white mr-1">Continuar</a>
                  <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-outline-secondary btn-success text-white">Editar</a>
                </div>
                <small class="text-muted">{{ $post->created_at->format('d/m/y') }}</small>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    {{--para sirve el paginate(numero) que está en FrontController en la función index--}}
    {{ $posts->links() }}
  </div>
@endsection