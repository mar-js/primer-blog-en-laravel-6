@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{ $post->image }}" class="card-img" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <p class="card-text">{{ $post->body }}.</p>
                    <p class="card-text"><small class="text-muted">{{ $post->created_at->format('d/m/y') }}</small></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
